"""Tests for the common module."""
import json
from pathlib import Path
import typing
from unittest import TestCase

from cki_jira import common


class TestFixVersions(TestCase):
    """Tests for the FixVersions."""

    # Everything that is not marked archived 2 Oct 2024.
    # https://issues.redhat.com/rest/api/2/project/RHEL/versions
    FIX_VERSIONS = [
        'rhel-6-els',
        'rhel-7.4',
        'rhel-7.6',
        'rhel-7.6.z',
        'rhel-7.7',
        'rhel-7.7.z',
        'rhel-7.9',
        'rhel-7.9.z',
        'rhel-7-els',
        'rhel-8.0.0',
        'rhel-8.1.0',
        'rhel-8.1.0.z',
        'rhel-8.2.0',
        'rhel-8.2.0.z',
        'rhel-8.4.0',
        'rhel-8.4.0.z',
        'rhel-8.6.0',
        'rhel-8.6.0.z',
        'rhel-8.7.0',
        'rhel-8.8.0',
        'rhel-8.8.0.z',
        'rhel-8.9.0',
        'rhel-8.9.0.z',
        'rhel-8.10',
        'rhel-8.10.z',
        'CentOS Stream 8',
        'CentOS Stream 9',
        'CentOS Stream 10',
        'rhel-9.0.0',
        'rhel-9.0.0.z',
        'rhel-9.1.0',
        'rhel-9.2.0',
        'rhel-9.2.0.z',
        'rhel-9.3.0',
        'rhel-9.3.0.z',
        'rhel-9.4',
        'rhel-9.4.z',
        'rhel-9.5',
        'rhel-9.5.z',
        'rhel-9.6',
        'rhel-9.6.z',
        'rhel-10.0.beta',
        'rhel-10.0',
        'rhel-10.0.z',
        'eln',
        'rhel-8.2.1.z',
        'rhel-10.1'
    ]

    def test_valid_fix_versions(self):
        """Returns an object with the expected attributes."""
        class FVTest(typing.NamedTuple):
            input: str
            product: str
            major: int
            minor: int
            cycle: str
            zstream: bool
            terminal: bool

        tests = [
            FVTest('rhel-8.9.0', 'rhel', 8, 9, 'release', False, False),
            FVTest('rhel-8.9.0.z', 'rhel', 8, 9, 'release', True, False),
            FVTest('rhel-11.2', 'rhel', 11, 2, 'release', False, False),
            FVTest('rhel-11.0.public.beta', 'rhel', 11, 0, 'beta', False, False),
            FVTest('rhel-12.0.public.beta.z', 'rhel', 12, 0, 'beta', False, False),
            FVTest('rhel-7.9', 'rhel', 7, 9, 'release', False, False),
            FVTest('rhel-8.10', 'rhel', 8, 10, 'release', False, False),
            # Special cases for terminal streams defined by common.TERMINAL_STREAMS.
            FVTest('rhel-6-els', 'rhel', 6, 10, 'release', True, True),
            FVTest('rhel-7.9.z', 'rhel', 7, 9, 'release', True, True),
            FVTest('rhel-8.10.z', 'rhel', 8, 10, 'release', True, True),
            # CentOS Streams.
            FVTest('CentOS Stream 9', 'centos', 9, 0, 'release', False, False),
            FVTest('CentOS Stream 10', 'centos', 10, 0, 'release', False, False),
            # These two are 'archived' on the RHEL project but we still have old metadata that
            # includes them.
            FVTest('rhel-8.0-alpha', 'rhel', 8, 0, 'alpha', False, False),
            FVTest('rhel-8.0-beta', 'rhel', 8, 0, 'beta', False, False),
            # 'eln' might be in the RHEL project.
            FVTest('eln', 'eln', 0, 0, 'release', False, False),
        ]

        for test in tests:
            with self.subTest(**test._asdict()):
                fix_version = common.FixVersion(test.input)
                for attr in ('product', 'major', 'minor', 'cycle', 'zstream', 'terminal'):
                    self.assertEqual(getattr(test, attr), getattr(fix_version, attr))

    def test_invalid_fix_versions(self):
        """Raises a ValueError on unexpected input."""
        tests = [
            'rhel-10',
            'rhel-8',
            'linux-1.2.3.z',
            'CentOS Stream 9.1',
            'rhel-6.els'  # this is no longer a valid fix version 🤷
        ]

        for bad_fix_version_str in tests:
            with self.subTest(bad_fix_version_str=bad_fix_version_str):
                with self.assertRaises(ValueError):
                    common.FixVersion(bad_fix_version_str)

    def test_sorting(self) -> None:
        """Works with sorted() to return the given FixVersions in the expected order."""
        fix_versions = [common.FixVersion(fix_version) for fix_version in self.FIX_VERSIONS]

        expected_rhel_order = [
            'rhel-6-els',
            'rhel-7.4',
            'rhel-7.6',
            'rhel-7.6.z',
            'rhel-7.7',
            'rhel-7.7.z',
            'rhel-7.9',
            'rhel-7.9.z',
            'rhel-7-els',
            'rhel-8.0.0',
            'rhel-8.1.0',
            'rhel-8.1.0.z',
            'rhel-8.2.0',
            'rhel-8.2.0.z',
            'rhel-8.2.1.z',
            'rhel-8.4.0',
            'rhel-8.4.0.z',
            'rhel-8.6.0',
            'rhel-8.6.0.z',
            'rhel-8.7.0',
            'rhel-8.8.0',
            'rhel-8.8.0.z',
            'rhel-8.9.0',
            'rhel-8.9.0.z',
            'rhel-8.10',
            'rhel-8.10.z',
            'rhel-9.0.0',
            'rhel-9.0.0.z',
            'rhel-9.1.0',
            'rhel-9.2.0',
            'rhel-9.2.0.z',
            'rhel-9.3.0',
            'rhel-9.3.0.z',
            'rhel-9.4',
            'rhel-9.4.z',
            'rhel-9.5',
            'rhel-9.5.z',
            'rhel-9.6',
            'rhel-9.6.z',
            'rhel-10.0.beta',
            'rhel-10.0',
            'rhel-10.0.z',
            'rhel-10.1'
        ]

        expected_centos_order = [
            'CentOS Stream 8',
            'CentOS Stream 9',
            'CentOS Stream 10'
        ]

        expected_eln_order = ['eln']

        self.assertEqual(
            sorted(fv for fv in fix_versions if fv.product == 'rhel'),
            expected_rhel_order
        )

        self.assertEqual(
            sorted(fv for fv in fix_versions if fv.product == 'centos'),
            expected_centos_order
        )

        self.assertEqual(
            sorted(fv for fv in fix_versions if fv.product == 'eln'),
            expected_eln_order
        )

    def test_bad_sorting(self) -> None:
        """Raises an exception on the wrong type or product."""
        rhel_fix_version = common.FixVersion('rhel-10.0')
        centos_fix_version = common.FixVersion('CentOS Stream 10')

        with self.assertRaises(TypeError):
            assert rhel_fix_version < 10

        with self.assertRaises(TypeError):
            assert rhel_fix_version < 'rhel-8.9.0'

        with self.assertRaises(ValueError):
            assert rhel_fix_version < centos_fix_version


class TestGitlabURL(TestCase):
    """Tests for the GitlabURL class."""

    def test_gitlab_url_good(self):
        """Returns a GitlabURL object with the expected attributes."""
        class URLTest(typing.NamedTuple):
            """Input string and expected GitlabURL attributes."""
            input_str: str
            base_url: str
            hostname: str
            namespace: str = ''
            project: str = ''
            type: str = ''
            id: int = 0
            view: str = ''
            reference: typing.Union[str, None] = None

        tests = [
            URLTest(
                input_str='https://gitlab.com/group/project/-/merge_requests/123',
                base_url='https://gitlab.com',
                hostname='gitlab.com',
                namespace='group/project',
                project='project',
                type='merge_requests',
                id=123,
                view='',
                reference='group/project!123'
            ),
            URLTest(
                input_str='https://gitlab.com/bip/boop/-/issues/555',
                base_url='https://gitlab.com',
                hostname='gitlab.com',
                namespace='bip/boop',
                project='boop',
                type='issues',
                id=555,
                view='',
                reference='bip/boop#555'
            ),
            URLTest(
                input_str='https://gitlab.com/bip/boop/-/pipelines/6254364',
                base_url='https://gitlab.com',
                hostname='gitlab.com',
                namespace='bip/boop',
                project='boop',
                type='pipelines',
                id=6254364,
                view='',
                reference=None
            ),
            URLTest(
                input_str=('https://gitlab.internal.redhat.com/redhat/centos-stream/src/kernel/'
                           'centos-stream-9/-/pipelines/1442101711'),
                base_url='https://gitlab.internal.redhat.com',
                hostname='gitlab.internal.redhat.com',
                namespace='redhat/centos-stream/src/kernel/centos-stream-9',
                project='centos-stream-9',
                type='pipelines',
                id=1442101711,
                view='',
                reference=None
            ),
        ]

        attributes = ('base_url', 'hostname', 'namespace', 'project', 'type', 'id', 'view',
                      'reference')

        for test in tests:
            with self.subTest(**test._asdict()):
                test_url = common.GitlabURL(test.input_str)
                self.assertEqual(test_url, test.input_str)
                for attrib in attributes:
                    self.assertEqual(getattr(test_url, attrib), getattr(test, attrib))

    def test_gitlab_url_bad(self):
        """Raises a ValueError when a URL is not recognized."""
        test_inputs = (
            'https://google.com',
            'group/project!999'
        )

        for test_str in test_inputs:
            with self.assertRaises(ValueError):
                common.GitlabURL(test_str)


class TestJiraKey(TestCase):
    """Tests for the common.JiraKey class."""

    def test_jira_key(self) -> None:
        """Returns a JiraKey instance with the expected attribute values."""
        test_key = common.JiraKey('RHEL-123')
        self.assertEqual(test_key.project, 'RHEL')
        self.assertEqual(test_key.id, 123)

        test_key = common.JiraKey('OSCI-999')
        self.assertEqual(test_key.project, 'OSCI')
        self.assertEqual(test_key.id, 999)

    def test_jira_key_bad(self) -> None:
        """Raises a ValueError on bad input."""
        with self.assertRaises(ValueError):
            common.JiraKey('RHEL123')

        with self.assertRaises(ValueError):
            common.JiraKey('RHEL')


class TestBugzillaID(TestCase):
    """Tests for the common.BugzillaID class."""

    def test_bugzilla_id(self) -> None:
        """Returns a BugzillaID instance for any numeric value between 1 and 99999999."""
        good_values = [123, '123', 1, 99999999]

        for good_value in good_values:
            with self.subTest(good_value=good_value):
                test_bugzilla_id = common.BugzillaID(good_value)
                self.assertIsInstance(test_bugzilla_id, common.BugzillaID)
                self.assertEqual(test_bugzilla_id, int(good_value))

        bad_values = [0, 100000000, 'RHEL-123', 'CVE-2024-11561', -666]

        for bad_value in bad_values:
            with self.subTest(bad_value=bad_value):
                with self.assertRaises(ValueError):
                    common.BugzillaID(bad_value)


class TestCveID(TestCase):
    """Tests for the common.CveID class."""

    def test_cve_id(self) -> None:
        """Returns a CveID instance for any string that matches the defs.CVE_ID_REGEX."""
        good_values = ['CVE-1999-123456', 'CVE-2026-0001', 'CVE-2024-5555']

        for good_value in good_values:
            with self.subTest(good_value=good_value):
                test_cve_id = common.CveID(good_value)
                self.assertIsInstance(test_cve_id, common.CveID)
                self.assertEqual(test_cve_id, good_value)

        bad_values = ['CVE-1993-123', 'cve-1999-123456', '2024-5555', 2023, 'RHEL-9999']

        for bad_value in bad_values:
            with self.subTest(bad_value=bad_value):
                with self.assertRaises(ValueError):
                    common.CveID(bad_value)


class TestTrackerLinks(TestCase):
    """Tests for the common.TrackerLinks class."""

    def test_get_bz_issuelinks(self) -> None:
        """Returns an IssueLinks dict."""
        test_dict = json.loads(Path('tests/assets/bz2218844.json').read_text())

        self.assertEqual(
            common.TrackerLinks.from_bz(test_dict),
            common.TrackerLinks(blocks=(2209174,))
        )

        test_dict['blocks'].append(2209174)
        self.assertEqual(
            common.TrackerLinks.from_bz(test_dict),
            common.TrackerLinks(blocks=(common.BugzillaID(2209174),))
        )

    def test_get_jira_issuelinks(self) -> None:
        """Returns an IssueLinks dict."""
        test_dict = json.loads(Path('tests/assets/RHEL-45232.json').read_text())
        links_list = test_dict['fields']['issuelinks']

        self.assertEqual(
            common.TrackerLinks.from_jira_links(links_list),
            common.TrackerLinks(
                blocks=(common.JiraKey('RHEL-45230'),),
                is_duplicated_by=(common.JiraKey('RHEL-45231'),)
            )
        )

        links_list.append(links_list[0])
        self.assertEqual(
            common.TrackerLinks.from_jira_links(links_list),
            common.TrackerLinks(
                blocks=(common.JiraKey('RHEL-45230'),),
                is_duplicated_by=(common.JiraKey('RHEL-45231'),)
            )
        )


class TestTestingStatus(TestCase):
    """Tests for the common.TestingStatus class."""

    def test_from_cf_verified(self) -> None:
        """Maps the cf_verified list values into a defs.TestingStatus member, or None."""
        self.assertIsNone(common.TestingStatus.from_cf_verified([]))
        self.assertEqual(
            common.TestingStatus.from_cf_verified(['FailedQA']),
            common.TestingStatus.FAIL
        )
        self.assertEqual(
            common.TestingStatus.from_cf_verified(['Tested']),
            common.TestingStatus.PASS
        )


class TestTrackerUser(TestCase):
    """Tests for the common.TestingStatus class."""

    def test_bugzilla_user(self) -> None:
        """Returns None if the input is empty."""
        self.assertIsNone(common.TrackerUser.from_bz_user({}))

        input_dict = {
            'real_name': 'Cool User',
            'email': 'cool_user@example.com',
            'name': 'cool_user_username'
        }

        self.assertEqual(
            common.TrackerUser.from_bz_user(input_dict),
            common.TrackerUser(
                name='Cool User',
                email='cool_user@example.com',
                username='cool_user_username'
            )
        )

    def test_jira_user(self) -> None:
        """Returns None if the input is empty, or a TicketUser."""
        self.assertIsNone(common.TrackerUser.from_jira_user({}))

        input_dict = {
            'displayName': 'Cool User',
            'name': 'cool_user_username'
        }

        # Handles a missing `emailAddress` field.
        self.assertEqual(
            common.TrackerUser.from_jira_user(input_dict),
            common.TrackerUser(name='Cool User', email='', username='cool_user_username')
        )

        # Finds the email address.
        input_dict['emailAddress'] = 'cool_user@example.com'

        self.assertEqual(
            common.TrackerUser.from_jira_user(input_dict),
            common.TrackerUser(
                name='Cool User',
                email='cool_user@example.com',
                username='cool_user_username'
            )
        )


class TestTrackerPriority(TestCase):
    """Tests for the TrackerPriority OrderedEnum + MultiValueEnum."""

    def test_tracker_priority_from_jira(self) -> None:
        """Returns the expected TrackerPriority for the given input."""
        self.assertIsNone(common.TrackerPriority.from_jira({}))

        priority_dict = {
            'iconUrl': 'https://issues.redhat.com/images/icons/priorities/medium.svg',
            'id': '10200',
            'name': 'Normal',
            'self': 'https://issues.redhat.com/rest/api/2/priority/10200'
        }

        self.assertIs(
            common.TrackerPriority.from_jira(priority_dict),
            common.TrackerPriority.NORMAL
        )

        priority_dict = {
            'iconUrl': 'https://issues.redhat.com/images/icons/priorities/critical.svg',
            'id': '2',
            'name': '중요',
            'self': 'https://issues.redhat.com/rest/api/2/priority/2'
        }

        self.assertIs(
            common.TrackerPriority.from_jira(priority_dict),
            common.TrackerPriority.CRITICAL
        )

    def test_tracker_priority_sorting(self) -> None:
        """TrackerPriority members are sorted into the expected order."""
        expected_order = [
            common.TrackerPriority.UNDEFINED,
            common.TrackerPriority.MINOR,
            common.TrackerPriority.NORMAL,
            common.TrackerPriority.MAJOR,
            common.TrackerPriority.CRITICAL,
            common.TrackerPriority.BLOCKER
        ]

        priority_members = set(common.TrackerPriority.__members__.values())

        self.assertEqual(expected_order, sorted(priority_members))


class TestTrackerResolution(TestCase):
    """Tests for the TrackerResolution MultiValueEnum."""

    def test_tracker_resolution_from_jira(self):
        """Returns the expected TrackerResolution for the given input."""
        self.assertIsNone(common.TrackerResolution.from_jira({}))

        resolution_dict = {
            'self': 'https://issues.redhat.com/rest/api/2/resolution/1',
            'id': '1',
            'description': 'This issue is closed, and complete.',
            'name': 'Done'
        }

        self.assertIs(
            common.TrackerResolution.from_jira(resolution_dict),
            common.TrackerResolution.DONE
        )

        resolution_dict = {
            'self': 'https://issues.redhat.com/rest/api/2/resolution/1',
            'id': '1',
            'description': 'Práca na tomto príbehu bola dokončená.',
            'name': 'Hotovo'
        }

        self.assertIs(
            common.TrackerResolution.from_jira(resolution_dict),
            common.TrackerResolution.DONE
        )


class TestTrackerSeverity(TestCase):
    """Tests for the TrackerSeverity OrderedEnum + MultiValueEnum."""

    def test_tracker_severity_from_jira(self) -> None:
        """Returns the expected TrackerSeverity for the given input."""
        self.assertIsNone(common.TrackerSeverity.from_jira({}))

        severity_dict = {
            'disabled': False,
            'id': '26751',
            'self': 'https://issues.redhat.com/rest/api/2/customFieldOption/26751',
            'value': 'Important'
        }

        self.assertIs(
            common.TrackerSeverity.from_jira(severity_dict),
            common.TrackerSeverity.IMPORTANT
        )

        severity_dict = {
            'disabled': False,
            'id': '26752',
            'self': 'https://issues.redhat.com/rest/api/2/customFieldOption/26752',
            'value': 'Moderate'
        }

        self.assertIs(
            common.TrackerSeverity.from_jira(severity_dict),
            common.TrackerSeverity.MODERATE
        )

    def test_tracker_severity_sorting(self) -> None:
        """TrackerSeverity members are sorted into the expected order."""
        expected_order = [
            common.TrackerSeverity.LOW,
            common.TrackerSeverity.MODERATE,
            common.TrackerSeverity.IMPORTANT,
            common.TrackerSeverity.CRITICAL,
        ]

        severity_members = set(common.TrackerSeverity.__members__.values())

        self.assertEqual(expected_order, sorted(severity_members))


class TestTrackerStatus(TestCase):
    """Tests for the TrackerStatus MultiValueEnum."""

    def test_tracker_status_from_jira(self):
        """Returns the expected TrackerStatus for the given input."""
        self.assertIsNone(common.TrackerStatus.from_jira({}))

        status_dict = {
            'self': 'https://issues.redhat.com/rest/api/2/status/6',
            'description': 'Príbeh sa považuje za ukončený, riešenie je správne.'
                           ' Príbehy, ktoré sú zatvorené, je možné opäť otvoriť.',
            'iconUrl': 'https://issues.redhat.com/images/icons/statuses/closed.png',
            'name': 'Uzavretá',
            'id': '6',
            'statusCategory': {
                'self': 'https://issues.redhat.com/rest/api/2/statuscategory/3',
                'id': 3,
                'key': 'done',
                'colorName': 'success',
                'name': 'Hotová'
            }
        }

        self.assertIs(
            common.TrackerStatus.from_jira(status_dict),
            common.TrackerStatus.CLOSED
        )

        status_dict = {
            'self': 'https://issues.stage.redhat.com/rest/api/2/status/13521',
            'description': 'Collaborative work to understand what needs to be done,'
                           ' and confirm understanding with stakeholders.',
            'iconUrl': 'https://issues.stage.redhat.com/',
            'name': 'Planning',
            'id': '13521',
            'statusCategory': {
                'self': 'https://issues.stage.redhat.com/rest/api/2/statuscategory/2',
                'id': 2,
                'key': 'new',
                'colorName': 'default',
                'name': 'To Do'
            }
        }

        self.assertIs(
            common.TrackerStatus.from_jira(status_dict),
            common.TrackerStatus.PLANNING
        )


class TestTrackerType(TestCase):
    """Tests for the TrackerType MultiValueEnum."""

    def test_tracker_type_from_jira(self):
        """Returns the expected TrackerType for the given input."""
        self.assertIsNone(common.TrackerType.from_jira({}))

        type_dict = {
            'self': 'https://issues.redhat.com/rest/api/2/issuetype/1',
            'id': '1',
            'description': '',
            'iconUrl': 'https://issues.redhat.com/secure/viewavatar?size=xsmall&avatarId=13263&'
                       'avatarType=issuetype',
            'name': 'Chyba',
            'subtask': False,
            'avatarId': 13263
        }

        self.assertIs(
            common.TrackerType.from_jira(type_dict),
            common.TrackerType.BUG
        )

        type_dict = {
            'avatarId': 13267,
            'description': 'Created by Jira Software - do not edit or delete. Issue type '
                           'for a big user story that needs to be broken down.',
            'iconUrl': 'https://issues.redhat.com/secure/viewavatar?size=xsmall&avatarId=13267&'
                       'avatarType=issuetype',
            'id': '16',
            'name': 'Epic',
            'self': 'https://issues.redhat.com/rest/api/2/issuetype/16',
            'subtask': False
        }

        self.assertIs(
            common.TrackerType.from_jira(type_dict),
            common.TrackerType.EPIC
        )
