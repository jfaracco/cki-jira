"""Tests for the cki_jira.tracker module."""
from pathlib import Path
import typing
from unittest import TestCase

from cki_jira import common
from cki_jira import tracker


class TestTrackerConstruction(TestCase):
    """Tests of the constructor functionality."""

    PAYLOADS = {
        'RHEL-1559': tracker.IssueTracker,
        'RHEL-3981': tracker.BaseTracker,
        'RHEL-9234': tracker.IssueTracker,
        'RHEL-45230': tracker.IssueTracker,
        'RHEL-45231': tracker.IssueTracker,
        'RHEL-45232': tracker.IssueTracker,
        'RHEL-45233': tracker.IssueTracker,
        'RHEL-45234': tracker.IssueTracker,
        'RHEL-45235': tracker.IssueTracker,
        'bz2170513': tracker.IssueTracker,
        'bz2218844': tracker.IssueTracker,
        'bz2226832': tracker.IssueTracker,
        'cve-2024-40998': tracker.CveFlaw,
    }

    def test_construction(self) -> None:
        """Creates the expected class instances from the given assets."""
        for tracker_id, expected_class in self.PAYLOADS.items():
            json_path = f'tests/assets/{tracker_id}.json'

            with self.subTest(json_path=json_path, expected_class=expected_class):
                test_tracker = tracker.make_tracker(json_path)

                self.assertIsInstance(test_tracker, expected_class)


class TestTrackerBasics(TestCase):
    """Test with the most basic attribute data."""

    def test_bad_values(self) -> None:
        """Raises a TypeError when an unexpected parameter is passed."""
        # Something that can't be parsed by any parser.
        with self.assertRaises(ValueError):
            tracker.make_tracker({'moors': 'moops'})

        with self.assertRaises(ValueError):
            tracker.make_tracker({})

    def test_basic_bugzilla(self) -> None:
        """Returns basic default values for a bugzilla non-KWF tracker."""
        test_tracker = tracker.make_tracker({'id': 12345678})

        self.assertIsInstance(test_tracker, tracker.BaseTracker)

        self.assertEqual(test_tracker.component, '')
        self.assertEqual(test_tracker.id, common.BugzillaID(12345678))
        self.assertEqual(test_tracker.product, '')
        self.assertIs(test_tracker.type, common.TrackerType.BUG)
        self.assertEqual(test_tracker.url, 'https://bugzilla.redhat.com/12345678')

        self.assertIn('12345678', repr(test_tracker))

    def test_bare_bugzilla(self) -> None:
        """Returns default KWF bugzilla tracker values."""
        test_issue = tracker.make_tracker({
            'id': 12345678,
            'components': ['kernel'],
            'product': 'Red Hat Enterprise Linux 9',
            'type': common.TrackerType.BUG,
        })

        self.assertIsInstance(test_issue, tracker.IssueTracker)

        self.assertIsNone(test_issue.assignee)
        self.assertIsNone(test_issue.assumed_fix_version)
        self.assertEqual(test_issue.component, 'kernel')
        self.assertIsNone(test_issue.creator)
        self.assertEqual(test_issue.cve_ids, [])
        self.assertEqual(test_issue.description, '')
        self.assertIsNone(test_issue.developer)
        self.assertFalse(test_issue.embargoed)
        self.assertIsNone(test_issue.fix_version)
        self.assertEqual(test_issue.fixed_in_build, '')
        self.assertEqual(test_issue.flaw_ids, [])
        self.assertEqual(test_issue.id, common.BugzillaID(12345678))
        self.assertFalse(test_issue.is_zstream)
        self.assertFalse(test_issue.is_ystream)
        self.assertEqual(test_issue.keywords, [])
        self.assertEqual(test_issue.labels, [])
        self.assertEqual(test_issue.links, common.TrackerLinks())
        self.assertIsNone(test_issue.migrated_id)
        self.assertEqual(test_issue.pool_team, '')
        self.assertIsNone(test_issue.priority)
        self.assertEqual(test_issue.product, 'Red Hat Enterprise Linux 9')
        self.assertIsNone(test_issue.qa_contact)
        self.assertIsNone(test_issue.resolution)
        self.assertIsNone(test_issue.severity)
        self.assertIsNone(test_issue.status)
        self.assertEqual(test_issue.sub_components, [])
        self.assertEqual(test_issue.summary, '')
        self.assertIsNone(test_issue.testing_status)
        self.assertIs(test_issue.type, common.TrackerType.BUG)
        self.assertEqual(test_issue.url, 'https://bugzilla.redhat.com/12345678')

        self.assertEqual(test_issue.find_testing_tasks(test_issue, {}), [])
        self.assertEqual(test_issue.find_variant_trackers(test_issue, {}), [])
        self.assertIsNone(test_issue.find_ystream_tracker(test_issue, {}))
        self.assertEqual(test_issue.find_zstream_trackers(test_issue, {}), [])

        self.assertIn('12345678', repr(test_issue))

    def test_basic_jira(self) -> None:
        """Returns basic default values for a jira non-KWF tracker."""
        test_issue = tracker.make_tracker({'key': 'RHEL-123'})

        self.assertIsInstance(test_issue, tracker.BaseTracker)

        self.assertEqual(test_issue.component, '')
        self.assertEqual(test_issue.id, common.JiraKey('RHEL-123'))
        self.assertEqual(test_issue.product, 'Red Hat Enterprise Linux')
        self.assertIsNone(test_issue.type)
        self.assertEqual(test_issue.url, '')

        self.assertIn('RHEL-123', repr(test_issue))

    def test_bare_jira(self) -> None:
        """Returns default KWF jira values."""
        test_issue = tracker.make_tracker({
            'key': 'RHEL-123',
            'fields': {
                'components': [{'name': 'kernel'}],
                'issuetype': {'name': 'Bug'}
            }
        })

        self.assertIsInstance(test_issue, tracker.IssueTracker)

        self.assertIsNone(test_issue.assignee)
        self.assertIsNone(test_issue.assumed_fix_version)
        self.assertEqual(test_issue.component, 'kernel')
        self.assertIsNone(test_issue.creator)
        self.assertEqual(test_issue.cve_ids, [])
        self.assertEqual(test_issue.description, '')
        self.assertIsNone(test_issue.developer)
        self.assertFalse(test_issue.embargoed)
        self.assertIsNone(test_issue.fix_version)
        self.assertEqual(test_issue.fixed_in_build, '')
        self.assertEqual(test_issue.flaw_ids, [])
        self.assertEqual(test_issue.id, common.JiraKey('RHEL-123'))
        self.assertFalse(test_issue.is_zstream)
        self.assertFalse(test_issue.is_ystream)
        self.assertEqual(test_issue.keywords, [])
        self.assertEqual(test_issue.labels, [])
        self.assertEqual(test_issue.links, common.TrackerLinks())
        self.assertIsNone(test_issue.migrated_id)
        self.assertEqual(test_issue.pool_team, '')
        self.assertIsNone(test_issue.priority)
        self.assertEqual(test_issue.product, 'Red Hat Enterprise Linux')
        self.assertIsNone(test_issue.qa_contact)
        self.assertIsNone(test_issue.resolution)
        self.assertIsNone(test_issue.severity)
        self.assertIsNone(test_issue.status)
        self.assertEqual(test_issue.sub_components, [])
        self.assertEqual(test_issue.summary, '')
        self.assertIsNone(test_issue.testing_status)
        self.assertIs(test_issue.type, common.TrackerType.BUG)
        self.assertEqual(test_issue.url, '')

        self.assertEqual(test_issue.find_testing_tasks(test_issue, {}), [])
        self.assertEqual(test_issue.find_variant_trackers(test_issue, {}), [])
        self.assertIsNone(test_issue.find_ystream_tracker(test_issue, {}))
        self.assertEqual(test_issue.find_zstream_trackers(test_issue, {}), [])

        self.assertIn('RHEL-123', repr(test_issue))

    def test_mr_urls(self) -> None:
        """Parses the remote_links list and returns matching GitlabURL MR urls."""
        test_issue = tracker.IssueTracker(id=common.JiraKey('RHEL-123'))

        # Set some remote links.
        test_issue.remote_links = [
            'https://access.redhat.com/errata/RHSA-2023-151265',
            'https://gitlab.com/group/project/-/merge_requests/123',
            'https://gitlab.com/group/project/-/issues/5',
            'https://gitlab.com/group/project25/-/merge_requests/777',
            'https://gitlab.internal.redhat.com/group/project/-/merge_requests/999',
        ]

        mr123 = common.GitlabURL('https://gitlab.com/group/project/-/merge_requests/123')
        mr777 = common.GitlabURL('https://gitlab.com/group/project25/-/merge_requests/777')
        mr999 = common.GitlabURL(
            'https://gitlab.internal.redhat.com/group/project/-/merge_requests/999'
        )

        # Returns any gitlab MR urls by default.
        self.assertCountEqual(test_issue.mr_urls(), [mr123, mr777, mr999])

        # Returns the MR url with the matching hostname.
        self.assertCountEqual(test_issue.mr_urls(hostname='gitlab.com'), [mr123, mr777])

        # Returns the MR url with the matching namespace.
        self.assertCountEqual(
            test_issue.mr_urls(hostname='gitlab.com', namespace='group/project'),
            [mr123]
        )

    def test_ticket_new(self) -> None:
        """Loads the data from the given path."""
        path = Path('tests/assets/RHEL-1559.json')

        self.assertEqual(tracker.make_tracker(path).id, common.JiraKey('RHEL-1559'))

    def test_assumed_fix_version_no_cve(self) -> None:
        """Returns the original fix_version value."""
        test_issue = tracker.IssueTracker(
            id=common.JiraKey('RHEL-123'),
            fix_version=common.FixVersion('rhel-8.10.z')
        )

        self.assertEqual(
            test_issue.assumed_fix_version,
            test_issue.fix_version
        )

    def test_assumed_fix_version_with_cve(self) -> None:
        """Returns the fix_version if present, or first affects_version, or None."""
        # Returns the fix_version when it is present.
        test_issue = tracker.IssueTracker(
            id=common.JiraKey('RHEL-123'),
            affects_versions=[common.FixVersion('rhel-8.9.0.z')],
            fix_version=common.FixVersion('rhel-8.10.z'),
            cve_ids=[common.CveID('CVE-2024-1234')]
        )

        self.assertIs(
            test_issue.assumed_fix_version,
            test_issue.fix_version
        )

        # Returns the affects_version when the fix_version is not present.
        test_issue = tracker.IssueTracker(
            id=common.JiraKey('RHEL-123'),
            affects_versions=[common.FixVersion('rhel-8.9.0.z')],
            cve_ids=[common.CveID('CVE-2024-1234')]
        )

        self.assertIs(
            test_issue.assumed_fix_version,
            test_issue.affects_versions[0]
        )

        # Returns none when we have none.
        test_issue = tracker.IssueTracker(
            id=common.JiraKey('RHEL-123'),
            cve_ids=[common.CveID('CVE-2024-1234')]
        )

        self.assertIsNone(test_issue.assumed_fix_version)

    def test_wrong_jira_status(self):
        """ Test wrong status for a Jira issue. """
        test_issue_1 = tracker.make_tracker({
            'key': 'RHEL-123',
            'fields': {
                'components': [{'name': 'kernel'}],
                'issuetype': {'name': 'Bug'},
                'status': {'name': 'foo'}
            },
        })

        test_issue_2 = tracker.make_tracker({
            'key': 'RHEL-123',
            'fields': {
                'components': [{'name': 'kernel'}],
                'issuetype': {'name': 'Bug'},
                'status': {'bar': 'foo'}
            },
        })

        self.assertIs(test_issue_1.status, None)
        self.assertIs(test_issue_2.status, None)


class TestJiraIssueRHEL1559(TestCase):
    """Tests for the JiraIssue class."""

    def setUp(self):
        """Set up the test JiraIssue RHEL-1559.."""
        jira_json = 'tests/assets/RHEL-1559.json'
        self.test_issue = typing.cast(tracker.IssueTracker, tracker.make_tracker(jira_json))

    def test_jira_issue(self) -> None:
        """Has the expected attribute values."""
        test_issue = self.test_issue

        self.assertIsInstance(test_issue, tracker.IssueTracker)

        self.assertEqual(
            test_issue.assignee,
            common.TrackerUser(
                name='Red Hat Dev',
                email='',
                username='dev_username'
            )
        )

        self.assertEqual(
            test_issue.creator,
            common.TrackerUser(
                name='Red Hat Dev',
                email='',
                username='dev_username'
            )
        )

        self.assertEqual(
            test_issue.developer,
            common.TrackerUser(
                name='Arch HW x86 Triage Bot',
                email='',
                username='arch-hw-x86-triage'
            )
        )

        self.assertEqual(
            test_issue.qa_contact,
            common.TrackerUser(
                name='Cue Eee',
                email='',
                username='redhat-qe-username'
            )
        )

        expected_description = ('This is a test report for scripting.\r\n\r\n\xa0\r\n\r\n'
                                'Yes, update it.\r\n\r\n\xa0\r\n\r\n'
                                'Hello Prarit.')

        self.assertEqual(test_issue.affects_versions, [])
        self.assertEqual(test_issue.assumed_fix_version, common.FixVersion('rhel-8.10'))
        self.assertEqual(test_issue.commit_hashes, [])
        self.assertEqual(test_issue.component, 'kernel')
        self.assertEqual(test_issue.description, expected_description)
        self.assertEqual(test_issue.sub_components, ['Platform Enablement', 'x86_64'])
        self.assertEqual(test_issue.cve_ids, [])
        self.assertFalse(test_issue.embargoed)
        self.assertEqual(test_issue.fix_version, common.FixVersion('rhel-8.10'))
        self.assertEqual(test_issue.fixed_in_build, '')
        self.assertEqual(test_issue.flaw_ids, [])
        self.assertEqual(test_issue.id, common.JiraKey('RHEL-1559'))
        self.assertFalse(test_issue.is_zstream)
        self.assertTrue(test_issue.is_ystream)
        self.assertEqual(
            test_issue.links,
            common.TrackerLinks(blocks=('RHEL-123',), is_blocked_by=('RHEL-14726',))
        )
        self.assertIs(test_issue.type, common.TrackerType.BUG)
        self.assertEqual(test_issue.id, common.JiraKey('RHEL-1559'))
        self.assertCountEqual(test_issue.keywords, ['TestOnly'])
        self.assertEqual(test_issue.labels, [])
        self.assertIsNone(test_issue.migrated_id)
        self.assertEqual(test_issue.pool_team, 'sst_arch_hw')
        self.assertIs(test_issue.priority, common.TrackerPriority.UNDEFINED)
        self.assertEqual(test_issue.product, 'Red Hat Enterprise Linux')
        self.assertEqual(test_issue.resolution, common.TrackerResolution.DONE)
        self.assertIsNone(test_issue.severity)
        self.assertIs(test_issue.status, common.TrackerStatus.CLOSED)
        self.assertEqual(test_issue.summary, 'I hate cats.')
        self.assertIs(test_issue.testing_status, common.TestingStatus.REQUESTED)
        self.assertEqual(test_issue.url, 'https://issues.redhat.com/browse/RHEL-1559')

        self.assertIn('RHEL-1559', repr(test_issue))

    def test_find_testing_tasks(self) -> None:
        """Returns JiraIssue Tasks from the cache from the 'is blocked by' list."""
        test_issue = self.test_issue

        test_task1 = tracker.IssueTracker(
            id=common.JiraKey('RHEL-111'),
            labels=['KWF:TestingTask'],
            type=common.TrackerType.TASK
        )

        test_story2 = tracker.IssueTracker(
            id=common.JiraKey('RHEL-222'),
            labels=['KWF:TestingTask'],
            type=common.TrackerType.STORY
        )

        test_cache = {test_issue.id: test_issue for test_issue in [test_task1, test_story2]}

        # Fudge the test_issue's 'is blocked by' list.
        test_issue.links = common.TrackerLinks(
            is_blocked_by=(common.JiraKey('RHEL-111'), common.JiraKey('RHEL-222'))
        )

        self.assertEqual(
            tracker.IssueTracker.find_testing_tasks(test_issue, test_cache),
            [test_task1]
        )


class TestCveFlaw(TestCase):
    """Tests for a CVE flaw."""

    def test_basic_init(self) -> None:
        """Sets up a basic CveFlaw."""
        test_flaw = tracker.make_tracker({
            'id': 1234567,
            'product': 'Security Response',
            'components': ['vulnerability'],
            'summary': 'EMBARGOED CVE-1999-12345 kernel: flaw',
        })

        self.assertIsInstance(test_flaw, tracker.CveFlaw)

        self.assertEqual(test_flaw.cve_id, 'CVE-1999-12345')
        self.assertTrue(test_flaw.embargoed)
        self.assertFalse(test_flaw.major_incident)

        self.assertIn('CVE-1999-12345', repr(test_flaw))

    def test_bad_product(self) -> None:
        """Loading a bug that is not Security Response product or vulnerability component raises."""
        with self.assertRaises(TypeError):
            tracker.CveFlaw.from_bugzilla({
                'id': 1234567,
                'product': 'Red Hat Enterprise Linux 7',
                'components': ['vulnerability'],
                'summary': 'EMBARGOED CVE-1999-12345 kernel: flaw',
            })

    def test_bad_cve(self) -> None:
        """A CveFlaw must have a single cve_id."""
        with self.assertRaises(ValueError):
            tracker.CveFlaw.from_bugzilla({
                'id': 1234567,
                'product': 'Security Response',
                'components': ['vulnerability'],
                'summary': 'CVE-1999-12345 CVE-1999-55555 kernel: flaw',
            })

        with self.assertRaises(ValueError):
            tracker.CveFlaw.from_bugzilla({
                'key': 'RHEL-12345',
            })


class TestJiraIssueRHEL45230(TestCase):
    """RHEL-45230 is a ystream issue with an kernel-rt testing Task and two zstream clones."""

    def setUp(self):
        """Create the test issue and its cache."""
        keys = ('RHEL-45230', 'RHEL-45231', 'RHEL-45232', 'RHEL-45233', 'RHEL-45234', 'RHEL-45235')
        self.keys = [common.JiraKey(key) for key in keys]

        self.issue_cache = {
            key: typing.cast(
                tracker.IssueTracker, tracker.make_tracker(f'tests/assets/{key}.json')
            ) for
            key in keys
        }

        self.test_yissue = self.issue_cache['RHEL-45230']
        self.test_zissues = [self.issue_cache['RHEL-45233'], self.issue_cache['RHEL-45234']]
        self.test_variant_trackers = [self.issue_cache['RHEL-45232']]

    def test_find_testing_tasks_for_rhel45230(self) -> None:
        """Returns a list with JiraIssue Task RHEL-45232 from the cache."""
        expected_issue_list = [self.issue_cache['RHEL-45232']]

        self.assertCountEqual(
            tracker.IssueTracker.find_testing_tasks(self.test_yissue, self.issue_cache),
            expected_issue_list
        )

    def test_find_variant_trackers_for_rhel45230(self) -> None:
        """Returns a list with JiraIssue Task RHEL-45232 & RHEL-45235 from the cache."""
        self.assertCountEqual(
            tracker.IssueTracker.find_variant_trackers(self.test_yissue, self.issue_cache),
            self.test_variant_trackers
        )

    def test_find_ystream_trackers_for_rhel45230(self) -> None:
        """Returns an empty list as RHEL-45230 is a ystream issue."""
        self.assertIsNone(
            tracker.IssueTracker.find_ystream_tracker(self.test_yissue, self.issue_cache),
        )

    def test_find_zstream_trackers_for_rhel45230(self) -> None:
        """Returns a list with JiraIssue RHEL-45233 & RHEL-45234 from the cache."""
        self.assertCountEqual(
            tracker.IssueTracker.find_zstream_trackers(self.test_yissue, self.issue_cache),
            self.test_zissues
        )

    def test_find_ystream_trackers_for_rhel45233(self) -> None:
        """Returns a list with RHEL-45230."""
        test_zissue = self.issue_cache['RHEL-45233']

        self.assertEqual(
            tracker.IssueTracker.find_ystream_tracker(test_zissue, self.issue_cache),
            self.test_yissue
        )

    def test_find_ystream_trackers_for_rhel45234(self) -> None:
        """Returns a list with RHEL-45230."""
        test_zissue = self.issue_cache['RHEL-45234']

        self.assertEqual(
            tracker.IssueTracker.find_ystream_tracker(test_zissue, self.issue_cache),
            self.test_yissue
        )


class TestJiraIssueRHEL9234(TestCase):
    """RHEL-45230 is a ystream issue with an kernel-rt testing Task and two zstream clones."""

    def setUp(self):
        """Create the test issue and its cache."""
        jirakey = common.JiraKey('RHEL-9234')
        bzid = common.BugzillaID('2226832')
        cveid = common.CveID('CVE-1999-12345')

        self.issue_cache = {
            jirakey: typing.cast(
                tracker.IssueTracker,
                tracker.make_tracker('tests/assets/RHEL-9234.json')
            ),
            bzid: typing.cast(
                tracker.IssueTracker,
                tracker.make_tracker('tests/assets/bz2226832.json')
            ),
        }

        self.flaw_cache = {
            cveid: typing.cast(
                tracker.IssueTracker,
                tracker.make_tracker('tests/assets/bz2226889.json')
            ),
        }

        self.test_yissue = self.issue_cache[bzid]
        self.test_zissues = [self.issue_cache[jirakey]]
        self.test_cve = [self.flaw_cache[cveid]]

    def test_find_zstream_trackers_for_bz2226832(self) -> None:
        """Returns a list with JiraIssue RHEL-9234 from the cache."""
        self.assertCountEqual(
            tracker.IssueTracker.find_zstream_trackers(self.test_yissue, self.issue_cache),
            self.test_zissues
        )

    def test_find_ystream_trackers_for_rhel9234(self) -> None:
        """Returns 2226832 as it is the ystream for RHEL-9234."""
        test_zissue = self.issue_cache[common.JiraKey('RHEL-9234')]

        self.assertEqual(
            tracker.IssueTracker.find_ystream_tracker(test_zissue, self.issue_cache),
            self.test_yissue
        )

    def test_find_cves_trackers_for_rhel9234(self) -> None:
        """Returns CVE-1999-12345 flaw as it is the CVE for RHEL-9234."""
        test_zissue = self.issue_cache[common.JiraKey('RHEL-9234')]

        self.assertEqual(
            tracker.IssueTracker.find_cves_trackers(test_zissue, self.flaw_cache),
            self.test_cve
        )

    def test_find_cves_trackers_for_bz2226832(self) -> None:
        """Returns CVE-1999-12345 flaw as it is the CVE for BZ 2226832."""
        test_yissue = self.issue_cache[common.BugzillaID('2226832')]

        self.assertEqual(
            tracker.IssueTracker.find_cves_trackers(test_yissue, self.flaw_cache),
            self.test_cve
        )

    def test_find_ystream_trackers_for_bz2226832(self) -> None:
        """Returns None as 2226832 is a ystream tracker itself."""
        test_yissue = self.issue_cache[common.BugzillaID('2226832')]

        self.assertIsNone(
            tracker.IssueTracker.find_ystream_tracker(test_yissue, self.issue_cache)
        )

    def test_find_variant_trackers_for_bz2226832(self) -> None:
        """Returns a list with JiraIssue Task 2226832 from the cache."""

        # There are no tasks in this fixture, the return empty list.
        self.assertCountEqual(
            tracker.IssueTracker.find_variant_trackers(self.test_yissue, self.issue_cache),
            []
        )


class TestHelpers(TestCase):
    """Test the helper functions."""

    def test_str2bool(self) -> None:
        """Converts a 'True' or 'False' string into a bool, or blows up."""
        self.assertIs(tracker.str2bool('True'), True)
        self.assertIs(tracker.str2bool('False'), False)

        with self.assertRaises(ValueError):
            tracker.str2bool('tru')

    def test_jira_issue_parse_field(self) -> None:
        """Returns the expected field value."""
        # Returns the given default if field_data is falsy.
        self.assertEqual(tracker._parse_field({}, default='go wild!'), 'go wild!')

        # Uses the given factory function on the found value.
        self.assertEqual(
            tracker._parse_field({'beep': '555'}, factory=int, default=7, key='beep'),
            555
        )

    def test_get_components(self) -> None:
        """Returns a list of component strings."""
        self.assertEqual(tracker._get_bz_components({}), [])

        # No subcomponents.
        data = {'component': ['peanut']}
        self.assertEqual(tracker._get_bz_components(data), ['peanut'])

        # With sub components.
        data['sub_components'] = {'peanut': ['butter', 'jelly']}
        self.assertEqual(
            tracker._get_bz_components(data),
            ['peanut', 'butter', 'jelly']
        )

    def test_get_cves(self) -> None:
        """Returns the list of CVE ID strings parsed from the input."""
        summary = 'Hey I think the fix for CVE-2027-52151 broke my stuff'
        self.assertEqual(tracker._get_bz_cves(summary), [])

        summary = 'CVE-2024-44952 kernel: driver core: Fix uevent_show() vs driver detach race'
        self.assertEqual(tracker._get_bz_cves(summary), ['CVE-2024-44952'])

        summary = 'EMBARGOED CVE-2023-39191 kernel: eBPF'
        self.assertEqual(tracker._get_bz_cves(summary), ['CVE-2023-39191'])

    def test_get_bz_fix_version(self) -> None:
        """Returns a defs.FixVersion derived from the itr and ztr values, ha."""
        # No data, no FixVersion.
        self.assertIsNone(tracker._get_bz_fix_version({}))
        self.assertIsNone(tracker._get_bz_fix_version({'product': 'rhev'}))

        # rhel-6-els is identified by the product string :shrug:.
        data = {
            'product': 'Red Hat Enterprise Linux 6',
            'cf_internal_target_release': '---',
            'cf_zstream_target_release': ''
        }
        self.assertEqual(tracker._get_bz_fix_version(data), common.FixVersion('rhel-6-els'))

        # Something with an ITR.
        data = {
            'product': 'Red Hat Enterprise Linux 8',
            'cf_internal_target_release': '8.4.0',
            'cf_zstream_target_release': '---'
        }
        self.assertEqual(tracker._get_bz_fix_version(data), common.FixVersion('rhel-8.4.0'))

        # Something with a ZTR.
        data = {
            'product': 'Red Hat Enterprise Linux 9',
            'cf_internal_target_release': '',
            'cf_zstream_target_release': '9.2.0'
        }
        self.assertEqual(tracker._get_bz_fix_version(data), common.FixVersion('rhel-9.2.0.z'))

    def test_get_jira_affects_versions(self) -> None:
        """Returns a list of defs.FixVersion or string objects."""
        # No fields data.
        self.assertEqual(tracker._get_jira_affects_versions({}), [])

        # 'versions' has a mix of known and unknown FixVersion values.
        test_fields = {'versions': [
            {'self': 'https://issues.stage.redhat.com/rest/api/2/version/12414808',
             'id': '12414808',
             'description': 'PPSync [release:2566]',
             'name': 'rhel-9.5',
             'archived': False,
             'released': False},
            {'self': 'https://issues.redhat.com/rest/api/2/version/12403197',
             'id': '12403197',
             'description': 'PPSync [release:1918]',
             'name': 'rhel-8.EOL',
             'archived': True,
             'released': False}
        ]}

        self.assertCountEqual(
            tracker._get_jira_affects_versions(test_fields),
            [common.FixVersion('rhel-9.5'), 'rhel-8.EOL']
        )

    def test_make_issue_id(self) -> None:
        """Returns the input converted into an IssueID or None."""
        self.assertEqual(common.BugzillaID(1234567), tracker.make_tracker_id(1234567))
        self.assertEqual(common.JiraKey('RHEL-123'), tracker.make_tracker_id('RHEL-123'))
        self.assertIsNone(tracker.make_tracker_id('bz123'))

    def test_get_bz_remote_links(self) -> None:
        """Returns a list of URL strings."""
        test_dict = tracker.json.loads(Path('tests/assets/bz2218844.json').read_text())

        expected_urls = [
            'https://issues.redhat.com/browse/RHELPLAN-161304',
            'https://access.redhat.com/errata/RHSA-2023:6583',
            'https://gitlab.com/redhat/rhel/src/kernel/rhel-9/-/merge_requests/909'
        ]

        self.assertCountEqual(
            tracker._get_bz_remote_links(test_dict['external_bugs']),
            expected_urls
        )
