"""Base classes for Red Hat issue trackers bugzilla & jira."""
from dataclasses import dataclass
from dataclasses import field
import enum
from functools import singledispatch
import json
import pathlib
import re
import typing
import urllib.parse

from typing_extensions import Self

from cki_jira import defs
from cki_jira.common import BugzillaID
from cki_jira.common import CUSTOM_FIELDS
from cki_jira.common import CveID
from cki_jira.common import FixVersion
from cki_jira.common import IdType
from cki_jira.common import IssueID
from cki_jira.common import JiraKey
from cki_jira.common import TestingStatus
from cki_jira.common import TrackerID
from cki_jira.common import TrackerLinks
from cki_jira.common import TrackerPriority
from cki_jira.common import TrackerResolution
from cki_jira.common import TrackerSeverity
from cki_jira.common import TrackerStatus
from cki_jira.common import TrackerType
from cki_jira.common import TrackerUser
from cki_jira.protocol import BaseTrackerProtocol
from cki_jira.protocol import KwfFlawProtocol
from cki_jira.protocol import KwfIssueProtocol
from cki_jira.protocol import KwfTrackerProtocol

# Type aliases.
JsonDict: typing.TypeAlias = dict[str, typing.Any]
FlawCache: typing.TypeAlias = dict[CveID, 'CveFlaw']
TrackerCache: typing.TypeAlias = dict[IssueID, 'IssueTracker']

# Various constant values used by tracker.

# Jira label used by prodsec to reference to the "Flaw" ID in bugzilla.
FLAW_LABEL_PREFIX = 'flaw:bz#'

# Jira label applied to every KWF testing task created by automation.
KWF_TESTING_TASK_LABEL = 'KWF:TestingTask'

# Jira labels used to trigger automation to create & link a KWF variant testing task.
# See cki-project/kernel-workflow#694
# See redhat/centos-stream/src/kernel/documentation!739
KWF_TESTING_TASK_REQUESTED_LABELS = ('KWF:kernel-rt', 'KWF:kernel-64k', 'KWF:kernel-automotive')

# Regex used to match commit sha strings in the jira issue commit hashes customfield_12324041.
COMMIT_HASHES_REGEX = re.compile(r'([a-fA-F0-9]{12,40})\s?')


@dataclass(kw_only=True)
class BaseTracker(BaseTrackerProtocol):
    """A representation of a basic Red Hat issue tracker item."""

    id: TrackerID
    component: str = ''
    product: str = ''
    type: TrackerType | None = None
    url: str = ''

    _data: JsonDict = field(default_factory=dict, hash=False)

    def __repr__(self) -> str:
        """Describe yourself."""
        values_str = ', '.join(f'{k}={v}' for k, v in self.__repr_dict__().items())
        return f'{self.__class__.__name__}({values_str})'

    def __repr_dict__(self) -> dict[str, typing.Any]:
        """Just the facts."""
        return {
            'id': self.id,
            'type': self.type.value[0] if self.type else '???',
            'component': self.component if self.component else '???'
        }

    # Derived properties & methods.

    @staticmethod
    def find_active_tracker(
        tracker: 'IssueTracker',
        cache: TrackerCache,
        checked_ids: set[IssueID] | None = None
    ) -> 'BaseTrackerProtocol':
        """Return the tracker this tracker duplicates or was migrated to, or the original."""
        if not (next_id := tracker.links.duplicate_of or tracker.migrated_id):
            return tracker

        checked_ids = checked_ids or set()
        checked_ids.add(tracker.id)

        if next_tracker := cache.get(next_id):
            if next_tracker.id in checked_ids:
                raise RuntimeError(f'{next_tracker.id} is linked back to one of {checked_ids}!')

            return next_tracker.find_active_tracker(next_tracker, cache, checked_ids)

        return tracker


@dataclass(repr=False, kw_only=True)
class BaseKwfTracker(BaseTracker, KwfTrackerProtocol):
    # pylint: disable=too-many-instance-attributes
    """A representation of a KWF tracker."""

    description: str = ''
    summary: str = ''

    status: TrackerStatus | None = None
    resolution: TrackerResolution | None = None

    priority: TrackerPriority | None = None
    severity: TrackerSeverity | None = None

    assignee: TrackerUser | None = None
    creator: TrackerUser | None = None
    developer: TrackerUser | None = None
    qa_contact: TrackerUser | None = None
    pool_team: str = ''

    sub_components: list[str] = field(default_factory=list, hash=False)

    cve_ids: list[CveID] = field(default_factory=list, hash=False)
    embargoed: bool = False
    flaw_ids: list[IssueID] = field(default_factory=list, hash=False)
    migrated_id: IssueID | None = None

    keywords: list[str] = field(default_factory=list, hash=False)
    labels: list[str] = field(default_factory=list, hash=False)

    links: TrackerLinks = field(default_factory=TrackerLinks)

    _remote_links: list[str] | None = field(default=None, hash=False)

    def __repr_dict__(self) -> dict[str, typing.Any]:
        """Just the facts."""
        if self.status is TrackerStatus.CLOSED and self.resolution:
            status_str = f'{self.status.value[0]} ({self.resolution.value[0]})'
        else:
            status_str = self.status.value[0] if self.status else '???'
        return {
            'id': self.id,
            'type': self.type.value[0] if self.type else '???',
            'status': status_str,
            'component': self.component if self.component else '???',
            'cves': self.cve_ids or 'None'
        }

    @staticmethod
    def parse_bugzilla(raw_data: JsonDict) -> dict[str, typing.Any]:
        """Construct an IssueTracker from a bugzilla API response."""
        parsed_data = _parse_basic_bugzilla(raw_data)

        assignee = TrackerUser.from_bz_user(raw_data.get('assigned_to_detail', {}))
        components = _get_bz_components(raw_data)
        description = raw_data.get('description', '')
        pool_team_dict = raw_data.get('agile_team', {})
        priority_str = raw_data.get('priority', '').upper()
        resolution_str = raw_data.get('resolution', '').upper()
        severity_str = raw_data.get('severity', '').upper()
        status_str = raw_data.get('status', '').upper()
        summary_str = raw_data.get('summary', '')

        embargoed = summary_str.startswith('EMBARGOED') or raw_data.get('cf_embargoed') is True

        return parsed_data | {
            'assignee': assignee,
            'creator': TrackerUser.from_bz_user(raw_data.get('creator_detail', {})),
            'cve_ids': _get_bz_cves(summary_str) if summary_str else [],
            'developer': assignee,
            'description': description,
            'embargoed': embargoed,
            'links': TrackerLinks.from_bz(raw_data),
            'keywords': raw_data.get('keywords', []),
            'pool_team': pool_team_dict.get('name', ''),
            'priority': TrackerPriority[priority_str] if priority_str else None,
            'qa_contact': TrackerUser.from_bz_user(raw_data.get('qa_contact_detail', {})),
            '_remote_links': _get_bz_remote_links(raw_data.get('external_bugs', [])),
            'resolution': TrackerResolution[resolution_str] if resolution_str else None,
            'severity': TrackerSeverity[severity_str] if
            severity_str in TrackerSeverity.__members__ else None,
            'status': TrackerStatus[status_str] if status_str else None,
            'sub_components': components[1:],
            'summary': summary_str
        }

    @staticmethod
    def parse_jira(raw_data: JsonDict) -> dict[str, typing.Any]:
        # pylint: disable=too-many-locals
        """Construct a basic IssueTracker from the json API response."""
        parsed_data = _parse_basic_jira(raw_data)
        fields = raw_data.get('fields', {})

        assignee = _get_field(fields, 'assignee', factory=TrackerUser.from_jira_user)
        creator = _get_field(fields, 'creator', factory=TrackerUser.from_jira_user)
        cve_id = _get_field(fields, CUSTOM_FIELDS.cve_id)
        description = _get_field(fields, 'description', default='')
        developer = _get_field(fields, CUSTOM_FIELDS.developer, factory=TrackerUser.from_jira_user)
        embargoed_str = \
            _get_field(fields, CUSTOM_FIELDS.embargo_status, key='value', default='False')
        keywords = _get_field(fields, CUSTOM_FIELDS.keywords, key='value')
        labels = fields.get('labels') or []
        migrated_id = _get_field(fields, CUSTOM_FIELDS.bugzilla_bug,
                                 factory=BugzillaID, key='bugid')
        pool_teams = _get_field(fields, CUSTOM_FIELDS.pool_teams, key='value')
        priority = TrackerPriority.from_jira(fields.get('priority', {}))
        qa_contact = \
            _get_field(fields, CUSTOM_FIELDS.qa_contact, factory=TrackerUser.from_jira_user)
        resolution = TrackerResolution.from_jira(fields.get('resolution', {}))
        severity = TrackerSeverity.from_jira(fields.get(CUSTOM_FIELDS.severity, {}))
        status = TrackerStatus.from_jira(fields.get('status', {}))
        summary_str = fields.get('summary', '')

        sub_components = []
        if components := _get_field(fields, 'components', key='name'):
            sub_components = components[0].split(' / ')[1:]

        # Make the cves list from the customfield CVE ID plus any other CVE labels.
        cve_ids = [cve_id] if cve_id else []
        cve_ids.extend(label for label in labels if label.startswith('CVE-') and label != cve_id)

        # Get the IDs out of any "flaw" labels.
        flaw_labels = [label for label in labels if label.startswith(FLAW_LABEL_PREFIX)]
        flaw_ids = [make_tracker_id(label.removeprefix(FLAW_LABEL_PREFIX)) for label in flaw_labels]

        # Figure out if this is an embargoed issue.
        embargoed = summary_str.startswith('EMBARGOED') or str2bool(embargoed_str)

        return parsed_data | {
            'assignee': assignee,
            'creator': creator,
            'cve_ids': [CveID(cve_id) for cve_id in cve_ids],
            'developer': developer,
            'description': description,
            'embargoed': embargoed,
            'flaw_ids': flaw_ids,
            'links': TrackerLinks.from_jira_links(fields.get('issuelinks', [])),
            'keywords': keywords or [],
            'labels': labels,
            'migrated_id': migrated_id,
            'pool_team': pool_teams[0] if pool_teams else '',
            'priority': priority,
            'product': 'Red Hat Enterprise Linux' if parsed_data['id'].project == 'RHEL' else '',
            'qa_contact': qa_contact,
            'resolution': resolution,
            'severity': severity,
            'status': status,
            'sub_components': sub_components,
            'summary': summary_str
        }


@dataclass(repr=False, kw_only=True, unsafe_hash=True)
class CveFlaw(BaseKwfTracker, KwfFlawProtocol):
    """A representation of a CVE flaw."""

    cve_id: CveID
    major_incident: bool = False

    def __repr_dict__(self) -> dict[str, typing.Any]:
        """Just the facts."""
        repr_dict = super().__repr_dict__()
        return {
            'CVE': self.cve_id,
            'id': repr_dict['id'],
            'severity': self.severity.name if self.severity else '???',
            'status': repr_dict['status'],
            'embargoed': self.embargoed
        }

    @classmethod
    def from_bugzilla(cls, raw_data: JsonDict, **kwargs: typing.Any) -> Self:
        """Construct a new CveFlaw instance from the given bugzilla data."""
        parsed_data = BaseKwfTracker.parse_bugzilla(raw_data)

        if parsed_data['product'] != 'Security Response' or \
           parsed_data['component'] != 'vulnerability':
            raise TypeError(f'{cls.__name__} only supports Security Response trackers.')

        cve_ids = parsed_data['cve_ids']
        if len(cve_ids) != 1:
            raise ValueError(f'CveFlaw is expected to represent a single CVE, found: {cve_ids}.')

        parsed_data.update({
            'cve_id': cve_ids[0],
            'major_incident': raw_data.get('cf_major_incident', False)
        })

        return cls(**parsed_data, **kwargs)

    @classmethod
    def find_trackers(
        cls,
        cve_id: CveID,
        cache: TrackerCache,
        component: str | None = None,
        major_version: int | None = None
    ) -> list['IssueTracker']:
        """Return the sorted list of KWF trackers for the given CVE ID."""
        matching_trackers = [
            cache_tracker for cache_tracker in cache.values() if
            cve_id in cache_tracker.cve_ids
        ]

        if major_version:
            matching_trackers = [cve_tracker for cve_tracker in matching_trackers if
                                 cve_tracker.assumed_fix_version and
                                 cve_tracker.assumed_fix_version.major == major_version]

        if component:
            matching_trackers = [cve_tracker for cve_tracker in matching_trackers if
                                 cve_tracker.component == component]

        # matching_trackers may have pairs of trackers which are duplicates of each other. After
        # we call find_active_tracker on the duplicate it will (hopefully) resolve back to some
        # tracker already in the matching_trackers list. So use a set here as a means of
        # deduplicating our results.
        cve_trackers = {
            typing.cast('IssueTracker', cls.find_active_tracker(tracker, cache)) for
            tracker in matching_trackers
        }

        return IssueTracker.sort_trackers(cve_trackers)

    @classmethod
    def find_ystream_tracker(
        cls,
        cve_id: CveID,
        major_version: int,
        component: str,
        cache: TrackerCache
    ) -> 'IssueTracker | None':
        """Return the ystream tracker for the given CVE/major version/component from the cache."""
        same_major_trackers = cls.find_trackers(cve_id, cache, component, major_version)
        return same_major_trackers[0] if same_major_trackers else None

    @classmethod
    def find_zstream_trackers(
        cls,
        cve_id: CveID,
        major_version: int,
        component: str,
        cache: TrackerCache
    ) -> list['IssueTracker']:
        """Return the list of zstream trackers for the given CVE/major version/component."""
        same_major_trackers = cls.find_trackers(cve_id, cache, component, major_version)
        return [tracker for tracker in same_major_trackers if
                tracker.fix_version.zstream]  # type: ignore[union-attr]


@dataclass(repr=False, kw_only=True, unsafe_hash=True)
class IssueTracker(BaseKwfTracker, KwfIssueProtocol):
    """A representation of a Red hat issue tracker instance."""

    id: IssueID

    fix_version: FixVersion | None = None
    affects_versions: list[FixVersion | str] = field(default_factory=list, hash=False)

    fixed_in_build: str = ''
    testing_status: TestingStatus | None = None
    story_points: int = -1
    commit_hashes: list[str] = field(default_factory=list, hash=False)

    def __repr_dict__(self) -> dict[str, typing.Any]:
        """Just the facts."""
        return super().__repr_dict__() | {'fix_version': self.fix_version}

    @classmethod
    def from_bugzilla(cls, raw_data: JsonDict, **kwargs: typing.Any) -> Self:
        """Construct a new KwfIssueProtocol instance from the given bugzilla data."""
        parsed_data = BaseKwfTracker.parse_bugzilla(raw_data)

        parsed_data.update({
            'fix_version': _get_bz_fix_version(raw_data),
            'fixed_in_build': raw_data.get('cf_fixed_in', ''),
            'testing_status': TestingStatus.from_cf_verified(raw_data.get('cf_verified', []))
        })

        return cls(**parsed_data, **kwargs)

    @classmethod
    def from_jira(cls, raw_data: JsonDict, **kwargs: typing.Any) -> Self:
        """Construct a new KwfIssueProtocol instance from the given jira data."""
        parsed_data = BaseKwfTracker.parse_jira(raw_data)
        fields = raw_data.get('fields', {})

        affects_versions = _get_jira_affects_versions(fields)
        commit_hashes = COMMIT_HASHES_REGEX.findall(
            _get_field(fields, CUSTOM_FIELDS.commit_hashes, default='')
        )
        fix_versions = _get_field(fields, 'fixVersions', factory=FixVersion, key='name')
        fixed_in_build = _get_field(fields, CUSTOM_FIELDS.fixed_in_build, default='')
        preliminary_testing = _get_field(fields, CUSTOM_FIELDS.preliminary_testing,
                                         factory=TestingStatus, key='value')
        story_points = _get_field(fields, CUSTOM_FIELDS.story_points, default=-1)

        parsed_data.update({
            'affects_versions': affects_versions,
            'commit_hashes': commit_hashes,
            'fix_version': fix_versions[0] if fix_versions else None,
            'fixed_in_build': fixed_in_build,
            'story_points': story_points,
            'testing_status': preliminary_testing,
        })

        return cls(**parsed_data, **kwargs)

    @classmethod
    def find_variant_trackers(
        cls,
        tracker: 'IssueTracker',
        cache: TrackerCache
    ) -> list['IssueTracker']:
        """Return the list of variant trackers; this is KWF Testing Tasks and any CVE variants."""
        # Begin with the set of testing tasks.
        variant_trackers = set(cls.find_testing_tasks(tracker, cache))

        # If this is a CVE tracker, then include any others which have the same fix_version but
        # different component.
        if tracker.fix_version and tracker.cve_ids:
            major_version_tackers = CveFlaw.find_trackers(
                tracker.cve_ids[0], cache, None, tracker.fix_version.major
            )

            variant_trackers.update(
                cve_tracker for cve_tracker in major_version_tackers if
                cve_tracker.fix_version == tracker.fix_version and
                cve_tracker.component != tracker.component
            )

        return list(variant_trackers)

    @classmethod
    def find_ystream_tracker(
        cls,
        tracker: 'IssueTracker',
        cache: TrackerCache
    ) -> 'IssueTracker | None':
        """Return the ystream tracker from the cache if this is a zstream or non-terminal stream."""
        if not tracker.fix_version or not tracker.is_zstream or tracker.fix_version.terminal:
            return None

        # If the input tracker is a clone then just accept whatever it is a clone of as the ystream.
        if tracker.links.clone_of:
            if ystream_tracker := cache.get(tracker.links.clone_of):
                return typing.cast('IssueTracker', cls.find_active_tracker(ystream_tracker, cache))

        # If the input tracker is for a CVE then we can look up the ystream that way.
        if tracker.cve_ids:
            return CveFlaw.find_ystream_tracker(
                tracker.cve_ids[0], tracker.fix_version.major, tracker.component, cache
            )

        return None

    @classmethod
    def find_zstream_trackers(
        cls,
        tracker: 'IssueTracker',
        cache: TrackerCache
    ) -> list['IssueTracker']:
        """Return the list of possible zstreams from the cache if this is a NON-CVE ystream."""
        if not tracker.fix_version or tracker.is_zstream:
            return []

        if tracker.cve_ids:
            return CveFlaw.find_zstream_trackers(
                tracker.cve_ids[0], tracker.fix_version.major, tracker.component, cache
            )

        clones = [
            issue for issue in cache.values() if
            issue.id in tracker.links.is_cloned_by and
            issue.fix_version and issue.fix_version.major == tracker.fix_version.major
        ]

        return cls.sort_trackers(
            typing.cast('IssueTracker', cls.find_active_tracker(clone, cache)) for clone in clones
        )

    @staticmethod
    def find_cves_trackers(
        tracker: 'IssueTracker',
        cache: FlawCache
    ) -> list['CveFlaw']:
        """Return the list of possible CVEs for a cache."""
        return [cache[cve_id] for cve_id in tracker.cve_ids if cve_id in cache]

    @staticmethod
    def find_testing_tasks(tracker: 'IssueTracker', cache: TrackerCache) -> list['IssueTracker']:
        """Return the list of testing Tasks associated with this tracker."""
        blocked_by = [cache[key] for key in tracker.links.is_blocked_by]

        return [
            issue for issue in blocked_by if
            issue.type is TrackerType.TASK and
            KWF_TESTING_TASK_LABEL in issue.labels
        ]

    @staticmethod
    def sort_trackers(trackers: typing.Iterable['IssueTracker']) -> list['IssueTracker']:
        """Return the list of trackers sorted by their fix_version or first affects_version."""
        sorted_trackers = sorted(
            [fv_tracker for fv_tracker in trackers if fv_tracker.assumed_fix_version],
            key=lambda tracker: typing.cast(FixVersion, tracker.assumed_fix_version),
            reverse=True
        )

        return sorted_trackers + [tracker for tracker in trackers if tracker not in sorted_trackers]


def make_tracker_id(value: typing.Any) -> TrackerID | None:
    """Try to construct a IssueID from the input value and return it, or None on failure."""
    for id_type in typing.get_args(TrackerID):
        try:
            return id_type(value)  # type: ignore[no-any-return]
        except (TypeError, ValueError):
            continue

    return None


# Constructor methods.

def _parse_basic_bugzilla(raw_data: JsonDict) -> dict[str, typing.Any]:
    """Parse the basic details of a bugzilla bug from the json API response."""
    if any(key in raw_data for key in ('key', 'self')):
        raise ValueError('Input appears to be a jira payload.')

    if 'id' not in raw_data:
        raise ValueError('Input does not appear to be a bugzilla payload.')

    bz_id = BugzillaID(raw_data['id'])
    components = _get_bz_components(raw_data)

    return {
        'id': bz_id,
        '_data': raw_data,
        'component': components[0] if components else '',
        'product': raw_data.get('product', ''),
        'type': TrackerType.BUG,
        'url': f"https://{defs.BUGZILLA_HOST}/{bz_id}"
    }


def _parse_basic_jira(raw_data: JsonDict) -> dict[str, typing.Any]:
    """Parse the basic details of a jira issue from the json API response."""
    if 'key' not in raw_data:
        raise ValueError('Input does not appear to be a jira payload.')

    key = JiraKey(raw_data['key'])

    if url_str := raw_data.get('self'):
        api_url = urllib.parse.urlparse(url_str)
        url = f"{api_url.scheme}://{api_url.netloc}/browse/{key}"
    else:
        url = ''

    fields = raw_data.get('fields', {})

    if components := _get_field(fields, 'components', key='name'):
        component = components[0].split(' / ')[0]
    else:
        component = ''

    issue_type = TrackerType.from_jira(fields.get('issuetype', {}))

    return {
        'id': key,
        '_data': raw_data,
        'component': component,
        'product': 'Red Hat Enterprise Linux' if key.project == 'RHEL' else '',
        'type': issue_type,
        'url': url
    }


def _is_flaw(product: str, tracker_type: TrackerType) -> bool:
    """Return True if the given parameters indicate a Security Response flaw, otherwise False."""
    return tracker_type is TrackerType.BUG and product == 'Security Response'


def _is_kwf_issue(component: str, product: str, tracker_type: TrackerType) -> bool:
    """Return True if the given parameters indicate a KWF tracker, otherwise False."""
    return tracker_type in defs.KWF_TRACKER_TYPES and \
        component in defs.KWF_COMPONENTS and \
        product.startswith('Red Hat Enterprise Linux')


def _identify_data(raw_data: JsonDict) -> tuple[IdType | None, bool, dict[str, typing.Any]]:
    """Return the IdType of the data, whether it is KWF, and a dict with the basic values."""
    parsed_data: dict[str, typing.Any] = {}

    for parser_func in (_parse_basic_jira, _parse_basic_bugzilla):
        try:
            parsed_data = parser_func(raw_data)
            break
        except ValueError:
            continue

    if not parsed_data:
        return None, False, {}

    is_kwf = _is_kwf_issue(parsed_data['component'], parsed_data['product'], parsed_data['type'])
    is_flaw = _is_flaw(parsed_data['product'], parsed_data['type'])

    id_type = parsed_data['id'].__class__ if not is_flaw else CveID

    return id_type, is_kwf, parsed_data


BASE_CLASS = BaseTracker  # pylint: disable=invalid-name
KWF_CLASSES: dict[IdType, typing.Type[CveFlaw | IssueTracker]] = {
    BugzillaID: IssueTracker,
    CveID: CveFlaw,
    JiraKey: IssueTracker
}


@singledispatch
def make_tracker(
    file_path: str,
    base_class: typing.Type[BaseTracker] = BaseTracker,
    kwf_classes: dict[IdType, typing.Type[CveFlaw | IssueTracker]] | None = None,
    **kwargs: typing.Any
) -> BaseTracker | None:
    """Construct a Tracker from the json data in the given file path."""
    return _make_tracker_from_path(pathlib.Path(file_path), base_class, kwf_classes, **kwargs)


@make_tracker.register(pathlib.Path)
def _make_tracker_from_path(  # type: ignore[misc]
    file_path: pathlib.Path,
    base_class: typing.Type[BaseTracker] = BaseTracker,
    kwf_classes: dict[IdType, typing.Type[CveFlaw | IssueTracker]] | None = None,
    **kwargs: typing.Any
) -> BaseTracker | None:
    """Construct a Tracker from the json data in the given pathlib.Path."""
    raw_data = json.loads(file_path.read_text(encoding='UTF-8'))
    return _make_tracker_from_dict(raw_data, base_class, kwf_classes, **kwargs)


@make_tracker.register(dict)
def _make_tracker_from_dict(  # type: ignore[misc]
    raw_data: JsonDict,
    base_class: typing.Type[BaseTracker] = BaseTracker,
    kwf_classes: dict[IdType, typing.Type[CveFlaw | IssueTracker]] | None = None,
    **kwargs: typing.Any
) -> BaseTracker | None:
    """Construct a Tracker from the given raw_data using the given classes."""
    id_type, is_kwf, parsed_data = _identify_data(raw_data)

    if not id_type:
        raise ValueError('Input is not recognized by any known parser.')

    base_class = base_class or BASE_CLASS
    kwf_classes = kwf_classes or KWF_CLASSES

    if is_kwf or id_type is CveID:
        matching_class = kwf_classes[id_type]

        if id_type is JiraKey:
            return matching_class.from_jira(raw_data, **kwargs)  # type: ignore[union-attr]
        if id_type in (BugzillaID, CveID):
            return matching_class.from_bugzilla(raw_data, **kwargs)

    return base_class(**parsed_data, **kwargs)


# Payload parsing methods from jira.

def _parse_field(
    field_data: typing.Any,
    factory: typing.Callable[[typing.Any], typing.Any] | None = None,
    default: typing.Any = None,
    key: str | None = None
) -> typing.Any:
    """Get a value from the given field and optionally pass it through the factory function."""
    if not field_data:
        return default

    value = field_data[key] if key else field_data

    # Special case for converting string values to my silly IntEnums.
    if isinstance(factory, enum.EnumMeta) and issubclass(factory, enum.IntEnum):
        return factory[value.upper()]

    return factory(value) if factory else value  # type: ignore[call-arg]


def _get_field(
    fields_payload: JsonDict,
    field_name: str,
    factory: typing.Callable[[typing.Any], typing.Any] | None = None,
    default: typing.Any = None,
    key: str | None = None,
) -> typing.Any:
    # pylint: disable=too-many-arguments
    """
    Return the field value from the given data.

    fields_payload: the complete `fields` dict from the raw issue payload.
    field_name: the real name of the field to get.
    factory: function to pass the returned field value(s) through. Must take a single arg.
    default: default value to return if the field is not found or is None.
    key: the key inside the field data to get the value from; assumes field data is a dict.

    If the field data is a list then 'factory', 'default', and 'key' are applied to each item
    in the list and a list of results is returned.
    """
    field_data = fields_payload.get(field_name)

    if field_name not in fields_payload or field_data is None:
        return default

    if isinstance(field_data, list):
        return [_parse_field(item, factory, default, key) for item in field_data]

    return _parse_field(field_data, factory, default, key)


def str2bool(input_str: str) -> bool:
    """Convert a 'True' or 'False' string into a bool."""
    if input_str.lower() == 'false':
        return False

    if input_str.lower() == 'true':
        return True

    raise ValueError(f"Input '{input_str}' does not have an expected value.")


def _get_jira_affects_versions(fields_payload: JsonDict) -> list[FixVersion | str]:
    """Return the list of 'versions' values, cast as FixVersion if possible."""
    if not (version_strs := _get_field(fields_payload, 'versions', key='name')):
        return []

    versions: list[FixVersion | str] = []

    for version in version_strs:
        try:
            versions.append(FixVersion(version))
        except ValueError:
            versions.append(version)

    return versions


# Payload parsing methods for bugzilla.

def _get_bz_components(data: JsonDict) -> list[str]:
    """Return the tuple of component components for the given bugzilla payload."""
    # The python-bugzilla Bug object _rawdata is a slightly mangled version of the
    # real API response. The 'component' list is replaced by string and
    # 'components' is created to store the original value.
    if not (component_list := data.get('components') or data.get('component')):
        return []
    # Bugzilla returns a list of component strings but afaik there can only be one so this
    # ignores anything other than the first.
    base_component: str = component_list[0]

    component_parts = [base_component]

    if 'sub_components' in data:
        component_parts.extend(data['sub_components'].get(base_component, []))

    return component_parts


def _get_bz_cves(summary_str: str) -> list[CveID]:
    """Return a tuple of all CveIDs from the start of the bug's summary string."""
    cve_regex = re.compile(r'(EMBARGOED |)(?P<cve>CVE-\d{4}-\d{4,7})(?= )')
    start_pos = 0
    cves = []

    while cve := cve_regex.match(summary_str, start_pos):
        cves.append(CveID(cve.group('cve')))
        start_pos = cve.end() + 1

    return cves


def _get_bz_fix_version(data: JsonDict) -> FixVersion | None:
    """Return the FixVersion derived from the BZ's ITR & ZTR, maybe."""
    if not (product := data.get('product', '')):
        return None

    itr = data.get('cf_internal_target_release', '')
    ztr = data.get('cf_zstream_target_release', '')
    if itr == '---':
        itr = ''
    if ztr == '---':
        ztr = ''

    # A special case for a special product.
    if product == 'Red Hat Enterprise Linux 6':
        return FixVersion('rhel-6-els')
    if itr:
        return FixVersion(f'rhel-{itr}')
    if ztr:
        # Some ztr strings already have .z, some don't.
        return FixVersion(f'rhel-{ztr}.z') or FixVersion(f'rhel-{ztr}')
    return None


def _get_bz_remote_links(external_links: list[JsonDict]) -> list[str]:
    """Return the list of URLs parsed from a BZ's external_bugs field."""
    links: list[str] = []

    for link in external_links:
        link_id = link['ext_bz_bug_id']
        links.append(link['type']['full_url'].replace('%id%', link_id))

    return links
