"""Common class definitions."""
import enum
import re
import typing
from urllib.parse import urlparse

from typing_extensions import Self
from typing_extensions import override


# This would be a lot easier in 3.11+: https://docs.python.org/3.13/howto/enum.html#multivalueenum
class MultiValueEnum(enum.Enum):
    # pylint: disable=no-member,protected-access
    """An enum whose members can have multiple values."""

    @classmethod
    def _missing_(cls, value: typing.Any) -> enum.Enum:
        """Look up the value in member's tuple of values."""
        for member in cls._member_map_.values():
            if isinstance(member._value_, tuple):
                if value in member._value_:
                    return member

        raise ValueError(f'{value} is not a valid {cls.__name__}')

    @classmethod
    def from_jira(cls, jira_dict: dict[str, typing.Any]) -> Self | None:
        """Return the member which matches the payload 'id' or 'name', or None."""
        if not jira_dict:
            return None

        to_try: list[int | str] = []

        if type_id := jira_dict.get('id'):
            to_try.append(int(type_id))

        if type_name := jira_dict.get('name'):
            to_try.append(type_name)

        if type_name := jira_dict.get('value'):
            to_try.append(type_name)

        for value in to_try:
            try:
                return cls(value)
            except ValueError:
                continue

        return None


class OrderedEnum(enum.Enum):
    """An enum which supports value comparisons between members based on their index."""

    def __lt__(self, other: Self) -> bool:
        """Return True if self is less than other, otherwise False."""
        if self.__class__ is not other.__class__:
            raise TypeError(f'Cannot compare {self.__class__} to {other.__class__}.')

        members = list(self.__class__.__members__.values())

        return members.index(self) < members.index(other)


# FixVersion regex and class.

# Terminal streams.
# For each major version: a tuple giving the last minor version and list of FixVersion strings.
TERMINAL_STREAMS: dict[int, tuple[int, list[str]]] = {
    6: (10, ['rhel-6-els']),
    7: (9, ['rhel-7.9.z', 'rhel-7-els']),
    8: (10, ['rhel-8.10.z'])
}

BUGZILLA_ID_PATTERN = r'(?P<id>\d{1,8})'
BUGZILLA_ID_REGEX = re.compile(rf'^{BUGZILLA_ID_PATTERN}$')

CENTOS_FIX_VERSION_REGEX = re.compile(r'^CentOS Stream (?P<major>\d+)$')


class FixVersion(str):
    """Wrapper for a jira version string."""

    def __init__(self, fv_string: str) -> None:
        """Ensure we have a valid value."""
        super().__init__()

        self.product: typing.Literal['rhel', 'centos', 'eln'] = 'rhel'
        self.major: int = 0
        # These attributes are only really meaningful for the rhel product.
        self.minor: int = 0
        self.cycle: typing.Literal['alpha', 'beta', 'release'] = 'release'
        self.zstream: bool = False
        self.terminal: bool = False

        # Handle the ELN case.
        if fv_string == 'eln':
            self.product = 'eln'
            return

        # Handle the CentOS Stream case.
        if fv_match := CENTOS_FIX_VERSION_REGEX.match(fv_string):
            self.major = int(fv_match['major'])
            self.product = 'centos'
            return

        # The rest are RHEL.

        # Handle the special terminal streams.
        for major_ver, (minor_ver, fv_strings) in TERMINAL_STREAMS.items():
            if fv_string in fv_strings:
                self.major = major_ver
                self.minor = minor_ver
                self.zstream = True
                self.terminal = True
                return

        # The non-terminal RHEL streams.
        if fv_match := RHEL_FIX_VERSION_REGEX.match(fv_string):
            self.major = int(fv_match['major'])
            self.minor = int(fv_match['minor'].strip('.'))

            if 'alpha' in fv_string.lower():
                self.cycle = 'alpha'

            elif 'beta' in fv_string.lower():
                self.cycle = 'beta'

            # Alpha/Beta are never zstream >:(
            if self.cycle == 'release':
                self.zstream = bool(fv_match['zstream'])

            return

        # Blow up if we haven't recognized the value.
        raise ValueError(f"'{fv_string}' is not a recognized fixVersion string.")

    @override
    def __lt__(self, other: Self) -> bool:  # type: ignore[override]
        """Return True if 'other' has an older FixVersion value than self."""
        if not isinstance(other, self.__class__):
            raise TypeError(f'Cannot compare with {other} ({other.__class__})')

        if self.product != other.product:
            raise ValueError(f"Cannot compare '{self.product}' with '{other.product}'.")

        for comparison in ('major', 'minor', 'terminal', 'zstream'):
            self_val = getattr(self, comparison)
            other_val = getattr(other, comparison)

            if self_val != other_val:
                return bool(self_val < other_val)  # The bool makes things clear for mypy.

        for comparison in ('release', 'beta'):
            self_val = self.cycle == comparison
            other_val = other.cycle == comparison

            if self_val != other_val:
                return bool(self_val < other_val)  # The bool makes things clear for mypy.

        return False


# Gitlab URL regex and class.

GITLAB_PATH_REGEX = re.compile(r'^/(?P<namespace>.*)/-/(?P<type>\w+)/(?P<id>\d+)/?(?P<view>\w*)$')


class GitlabURL(str):
    # pylint: disable=too-many-instance-attributes
    """Simple wrapper for a Gitlab URL."""

    def __init__(self, url_str: str) -> None:
        """Blow up if this isn't a validish URL."""
        url = urlparse(url_str)
        gl_bits = GITLAB_PATH_REGEX.match(url.path)

        # If it can't find the scheme, netloc, and path then this is no good.
        if not all(item for item in url[0:3]) or not gl_bits:
            raise ValueError(f"'{url_str}' does not appear to be a valid gitlab URL.")

        self.base_url = f'{url.scheme}://{url.netloc}'
        self.hostname = url.netloc
        self.namespace = gl_bits.group('namespace')
        self.project = self.namespace.rsplit('/', 1)[-1]
        self.type = gl_bits.group('type')
        self.id = int(gl_bits.group('id'))
        self.view = gl_bits.group('view')

        match self.type:
            case 'merge_requests':
                ref_sep = '!'
            case 'issues':
                ref_sep = '#'
            case _:
                ref_sep = ''

        self.reference = f'{self.namespace}{ref_sep}{self.id}' if ref_sep else None


# Tracker ID regexes and classes.

# https://cve.mitre.org/cve/identifiers/syntaxchange.html
CVE_ID_REGEX = re.compile(r'^(?P<id>^CVE-\d{4}-\d{4,7})$')

JIRA_KEY_PATTERN = r'(?P<project>[A-Z][A-Z0-9]+)-([0-9]+)'
JIRA_KEY_REGEX = re.compile(rf'^{JIRA_KEY_PATTERN}$')

RHEL_FIX_VERSION_REGEX = \
    re.compile(r'^rhel-(?P<major>\d+)(?P<minor>\.\d+|-els)(?:(?P<zstream>.*\.z)|.*)$')


class BugzillaID(int):
    """Simple validation for a BZ int."""

    def __init__(self, value: int | str) -> None:
        """Make sure we have a number in what seems like a valid range."""
        if not BUGZILLA_ID_REGEX.match(str(value)) or int(value) < 1:
            raise ValueError(f"Input '{value}' is not a number between 1 and 99999999.")
        super().__init__()


class CveID(str):
    """A CVE ID."""

    def __init__(self, value: str) -> None:
        """Validate the value is a CVE ID."""
        if not isinstance(value, str) or not CVE_ID_REGEX.match(value):
            raise ValueError(f"Input '{value}' is not a valid CVE identifier.")
        super().__init__()


class JiraKey(str):
    """Simple class to represent a Jira Key."""

    def __init__(self, key_string: str) -> None:
        """Ensure the key_string is a validish jira key."""
        if not (match := JIRA_KEY_REGEX.match(key_string)):
            raise ValueError(f"Input '{key_string}' is not a valid Jira identifier.")
        self.project = match.group(1)
        self.id = int(match.group(2))
        super().__init__()


IssueID = JiraKey | BugzillaID
TrackerID = IssueID | CveID

IdType = typing.Type[BugzillaID] | typing.Type[CveID] | typing.Type[JiraKey]


# Classes used to represent various tracker attributes.

class JiraCustomField(typing.NamedTuple):
    """Mapping of pretty field names to customfield_* names."""

    preliminary_testing: str = 'customfield_12321540'
    testable_builds: str = 'customfield_12321740'
    release_blocker: str = 'customfield_12319743'
    internal_target_milestone: str = 'customfield_12321040'
    dev_target_milestone: str = 'customfield_12318141'
    qa_contact: str = 'customfield_12315948'
    developer: str = 'customfield_12315141'
    commit_hashes: str = 'customfield_12324041'
    pool_teams: str = 'customfield_12317259'
    reset_contacts: str = 'customfield_12322640'
    keywords: str = 'customfield_12323341'
    fixed_in_build: str = 'customfield_12318450'
    bugzilla_bug: str = 'customfield_12316840'
    story_points: str = 'customfield_12310243'
    cve_id: str = 'customfield_12324749'
    embargo_status: str = 'customfield_12324750'
    severity: str = 'customfield_12316142'


CUSTOM_FIELDS = JiraCustomField()

LinksTuple = tuple[IssueID, ...]

# Mapping of bugzilla link ID fields to jira issuelinks names.
ISSUE_LINKS_MAP = {
    'blocks': 'blocks',
    'blocks_all': 'blocks',
    'cf_clone_of': 'clones',
    'clone_ids': 'is_cloned_by',
    'depends_on': 'depends_on',
    'depends_on_all': 'depends_on',
    'dupe_of': 'duplicates',
}


class TrackerLinks(typing.NamedTuple):
    """Links to other trackers."""

    # https://issues.redhat.com/rest/api/2/issueLinkType

    account_is_impacted_by: LinksTuple = tuple()
    impacts_account: LinksTuple = tuple()
    is_blocked_by: LinksTuple = tuple()
    blocks: LinksTuple = tuple()
    is_caused_by: LinksTuple = tuple()
    causes: LinksTuple = tuple()
    is_cloned_by: LinksTuple = tuple()
    clones: LinksTuple = tuple()
    is_depended_on_by: LinksTuple = tuple()
    depends_on: LinksTuple = tuple()
    is_documented_by: LinksTuple = tuple()
    documents: LinksTuple = tuple()
    is_duplicated_by: LinksTuple = tuple()
    duplicates: LinksTuple = tuple()
    is_incorporated_by: LinksTuple = tuple()
    incorporates: LinksTuple = tuple()
    split_from: LinksTuple = tuple()
    split_to: LinksTuple = tuple()
    is_related_to: LinksTuple = tuple()
    relates_to: LinksTuple = tuple()
    is_triggered_by: LinksTuple = tuple()
    is_triggering: LinksTuple = tuple()

    @classmethod
    def from_bz(cls, raw_bug: dict[str, typing.Any]) -> Self:
        """Construct a new IssueLinks tuple from the input bug data."""
        links_dict: dict[str, list[IssueID]] = {name: [] for name in cls._fields}

        # Translate all the associated bug relationships into an IssueLinks.
        for bug_link_type, link_type in ISSUE_LINKS_MAP.items():
            if id_or_ids := raw_bug.get(bug_link_type):
                bz_ids: list[int] = id_or_ids if isinstance(id_or_ids, list) else [id_or_ids]

                for bz_id in bz_ids:
                    link_type_list = links_dict[link_type]

                    # Don't repeat IDs.
                    if bz_id in link_type_list:
                        continue

                    link_type_list.append(BugzillaID(bz_id))

        return cls(**{k: tuple(v) for k, v in links_dict.items()})

    @classmethod
    def from_jira_links(cls, links_data: list[dict[str, typing.Any]]) -> Self:
        """Construct a new IssueLinks tuple from the input jira issuelinks list."""
        links_dict: dict[str, list[IssueID]] = {name: [] for name in cls._fields}

        for link in links_data:
            if 'outwardIssue' in link:
                key = link['outwardIssue']['key']
                link_type = link['type']['outward']
            else:
                key = link['inwardIssue']['key']
                link_type = link['type']['inward']

            link_type_list = links_dict[link_type.replace(' ', '_')]

            # Don't repeat IDs.
            if key in link_type_list:
                continue

            link_type_list.append(JiraKey(key))

        return cls(**{k: tuple(v) for k, v in links_dict.items()})

    @property
    def clone_of(self) -> IssueID | None:
        """Return the first item in the 'clones' list, or None."""
        return self.clones[0] if self.clones else None

    @property
    def duplicate_of(self) -> IssueID | None:
        """Return the first item in the 'duplicates' list, or None."""
        return self.duplicates[0] if self.duplicates else None


class TestingStatus(enum.Enum):
    """Representation of an issue's testing status."""

    PASS = 'Pass'
    FAIL = 'Fail'
    REQUESTED = 'Requested'

    @classmethod
    def from_cf_verified(cls, cf_verified: list[str]) -> 'TestingStatus | None':
        """Return the TestingStatus of a BZ derived from the cf_verified strings, or None."""
        if 'FailedQA' in cf_verified:
            return cls.FAIL
        if 'Tested' in cf_verified:
            return cls.PASS
        return None


class TrackerPriority(OrderedEnum, MultiValueEnum):
    """Possible priority of a tracker."""

    # https://issues.redhat.com/rest/api/2/priority
    UNDEFINED = 'Undefined', 10300
    MINOR = 'Minor', 4
    NORMAL = 'Normal', 10200
    MAJOR = 'Major', 3
    CRITICAL = 'Critical', 2
    BLOCKER = 'Blocker', 1
    # https://bugzilla.redhat.com/page.cgi?id=fields.html#priority
    UNSPECIFIED = UNDEFINED
    LOW = MINOR
    MEDIUM = NORMAL
    HIGH = MAJOR
    URGENT = CRITICAL


class TrackerResolution(MultiValueEnum):
    """Possible resolution of an TrackerStatus.CLOSED tracker."""

    # https://issues.redhat.com/secure/ShowConstantsHelp.jspa?decorator=popup#ResolutionTypes
    # https://issues.redhat.com/rest/api/2/resolution
    DONE = 'Done', 1
    WONT_DO = "Won't Do", 10000
    CANNOT_REPRODUCE = 'Cannot Reproduce', 5
    CANT_DO = "Can't Do", 4
    DUPLICATE = 'Duplicate', 10700
    NOT_A_BUG = 'Not a Bug', 10802
    DONE_ERRATA = 'Done-Errata', 10803
    MIRROR_ORPHAN = 'MirrorOrphan', 10804
    OBSOLETE = 'Obsolete', 10900
    TEST_PENDING = 'Test Pending', 11100
    # https://bugzilla.redhat.com/page.cgi?id=fields.html#resolution
    CURRENTRELEASE = 'CURRENTRELEASE'
    ERRATA = DONE_ERRATA
    NOTABUG = NOT_A_BUG
    WONTFIX = WONT_DO
    CANTFIX = CANT_DO
    DEFERRED = 'DEFERRED'
    INSUFFICIENT_DATA = CANNOT_REPRODUCE
    NEXTRELEASE = 'NEXTRELEASE'
    RAWHIDE = 'RAWHIDE'
    UPSTREAM = 'UPSTREAM'
    WORKSFORME = 'WORKSFORME'
    EOL = OBSOLETE
    MIGRATED = 'MIGRATED'
    COMPLETED = DONE


class TrackerSeverity(OrderedEnum, MultiValueEnum):
    """Possible severity of a tracker."""

    # https://access.redhat.com/security/updates/classification
    LOW = 'Low', 26753
    MODERATE = 'Moderate', 26752
    IMPORTANT = 'Important', 26751
    CRITICAL = 'Critical', 26750
    # For Jira.
    MINOR = LOW
    NORMAL = MODERATE
    MAJOR = IMPORTANT
    # For bugzilla: https://bugzilla.redhat.com/page.cgi?id=fields.html#severity
    MEDIUM = MODERATE
    HIGH = IMPORTANT
    URGENT = CRITICAL


class TrackerStatus(MultiValueEnum):
    """Possible status of a tracker."""

    # https://issues.redhat.com/rest/api/2/project/12332745/statuses
    NEW = 'New', 10016
    PLANNING = 'Planning', 13521
    IN_PROGRESS = 'In Progress', 10018
    INTEGRATION = 'Integration', 18721
    RELEASE_PENDING = 'Release Pending', 15735
    CLOSED = 'Closed', 6
    # https://bugzilla.redhat.com/page.cgi?id=fields.html#bug_status
    ASSIGNED = PLANNING
    POST = IN_PROGRESS
    MODIFIED = INTEGRATION
    ON_DEV = 'ON_DEV'
    ON_QA = 'ON_QA'
    VERIFIED = 'VERIFIED'


class TrackerType(MultiValueEnum):
    """Possible issuetype of a tracker."""

    # The value tuple contains the issuetype name and numeric ID value.
    # https://issues.redhat.com/rest/api/2/project/12332745
    TASK = 'Task', 3
    BUG = 'Bug', 1
    STORY = 'Story', 17
    EPIC = 'Epic', 16
    SUB_TASK = 'Sub-task', 5
    VULNERABILITY = 'Vulnerability', 12207
    WEAKNESS = 'Weakness', 12206


class TrackerUser(typing.NamedTuple):
    """User associated with a tracker."""

    name: str
    email: str
    username: str

    @classmethod
    def from_bz_user(cls, user_dict: dict[str, typing.Any]) -> Self | None:
        """Construct a TrackerUser from the bugzilla input, or return None if empty."""
        if not user_dict:
            return None

        return cls(
            name=user_dict.get('real_name', ''),
            email=user_dict.get('email', ''),
            username=user_dict.get('name', ''),
        )

    @classmethod
    def from_jira_user(cls, user_dict: dict[str, typing.Any]) -> Self | None:
        """Construct a TrackerUser from the jira input, or return None if empty."""
        if not user_dict:
            return None

        return cls(
            name=user_dict.get('displayName', ''),
            email=user_dict.get('emailAddress', ''),
            username=user_dict.get('name', ''),
        )
