Name: kwf-lib
Version: 1.0.0
Release: %autorelease
Summary: A collection of classes, functions and scripts related to kernel workflow.

License: GPL-3.0-or-later
# TODO: Rename it to kwf_lib
URL: https://gitlab.com/cki-project/cki-jira

# TODO: Rename it to kwf_lib
Source0: https://gitlab.com/cki-project/cki-jira/-/archive/v%{version}/cki-jira-v%{version}.tar.gz

BuildArch: noarch
BuildRequires: python3-devel

%description
A Python library that contains classes, functions and scripts to deal with
Jira issues, Bugzillas, CVEs and other important stuff related to kernel
workflow.

%prep
%autosetup

%generate_buildrequires
%pyproject_buildrequires -e spec

%build
%pyproject_wheel

%install
%pyproject_install
# TODO: Rename it to kwf_lib
%pyproject_save_files cki_jira

%check
%tox

%files  -f %{pyproject_files}
%license LICENSE
%doc README.md LICENSE
# TODO: Rename it to kwf_lib
%{_bindir}/cki_jira

%changelog
%autochangelog
